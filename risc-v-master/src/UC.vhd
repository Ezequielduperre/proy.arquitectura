library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity UC is
    generic(
        opcode  :   integer :=  32
    );
    port(
        INSTR_i         :   in  std_logic_vector(opcode-1 downto 0);            -- instruccion de entrada
        Condbranch_o    :   out std_logic;                                      -- salto
        uncondbranch_o  :   out std_logic;                                      -- salto incondicional
        MemRead_o       :   out std_logic;                                      -- lectura de memoria
        MemtoReg_o      :   out std_logic;                                      -- memoria a registro
        ALU_con_o       :   out std_logic;              	         	-- control de la ALU
        MemWrite_o      :   out std_logic;                                      -- escritura de memoria
        ALUsrc_o        :   out std_logic;                                      -- selecciona entre un inmediato y un registro
        U_type_o        :   out std_logic;                                      -- permite utilizar lui
        Reg_W_o         :   out std_logic                                       -- escritura de registro                                     
    );
end UC;

architecture control_arch of UC is
    signal opcode_i:    std_logic_vector(6 downto 0);   --opcode de instruccion, hasta 7 bits
    
    begin
    opcode_i <= INSTR_i(6 downto 0);      -- se le otorga la parte que corresponde al opcode
     
    Condbranch_o    <= '1' when (opcode_i="1100111") else '0';
    uncondbranch_o  <= '1' when (opcode_i="1100011" or opcode_i="1101111") else '0';
    MemRead_o       <= '1' when (opcode_i="0000011") else '0';
    MemtoReg_o      <= '1' when (opcode_i="0000011") else '0';
    MemWrite_o      <= '1' when (opcode_i="0100011") else '0';
    ALUsrc_o        <= '1' when (opcode_i="0000011" or opcode_i = "0010011" or opcode_i= "0100011") else '0'; -- verificar jalr y jal 
    Reg_W_o         <= '1' when (opcode_i="0110011" or opcode_i="0000011" or opcode_i= "0010011") else '0';
    U_type_o        <= '1' when (opcode_i="0110111") else '0';

    ALU_con_o       <= '1' when (opcode_i="0110011") else '0';

end control_arch;