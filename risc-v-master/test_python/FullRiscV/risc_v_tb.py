import cocotb
from cocotb.clock import Clock
from cocotb.triggers import Timer, RisingEdge, FallingEdge, Edge, Event
from cocotb.result import TestFailure, TestError, ReturnValue, SimFailure

CLK_PERIOD = 10

@cocotb.coroutine
def Reset(dut):
    dut.RST_i <=  0
    yield Timer(CLK_PERIOD * 1)
    dut.RST_i  <= 1
    yield Timer(CLK_PERIOD * 10)
    dut.RST_i  <= 0

@cocotb.test()
def test(dut):
    """
    Description:
        Test RISC-V Simple Implementation
    """
    cocotb.fork(Clock(dut.CLK_i, CLK_PERIOD).start())
    
    yield Reset(dut)

    #yield Timer(CLK_PERIOD * 64)

    for i in range(64):
        yield RisingEdge(dut.CLK_i)

        print("Siguiente Instruccion",i)
        print("Señales de control")
        print("RegWrite =",dut.RegWrite.value)
        print("MemRead =",dut.MemRead.value)
        print("MemWrite =",dut.MemWrite.value)
        print("ALUSrc =",dut.ALUSrc.value)
        print("MemtoReg =",dut.MemtoReg.value)
        print("Branch =",dut.Branch.value)
        print("ALU_con =",dut.ALU_con.value)
        print("Señales internas")
        print("Instruction =",dut.Instruction.value)
        print("PCOUT =",dut.PCOUT.value)
        print("MUXtoPC =",dut.MUXtoPC.value)
        print("En =",dut.En.value)
        print("ReadDATA1toALU =",dut.ReadDATA1toALU.value)
        print("ReadDATA2toALU =",dut.ReadDATA2toALU.value)
        print("MUXtoREGISTERS =",dut.MUXtoREGISTERS.value)
        print("ImmGenOUT =",dut.ImmGenOUT.value)
        print("MuxtoALU =",dut.MuxtoALU.value)
        print("ALUresult =",dut.ALUresult.value)
        print("ZERO =",dut.ZERO.value)
        print("DataMEMtoMux =",dut.DataMEMtoMux.value)
        print("AND_OUT =",dut.AND_OUT.value)
        print("SHIFTED_LEFT =",dut.SHIFTED_LEFT.value)
        print("UC_MUX =",dut.UC_MUX.value)
        print("MUX_used_to_U_type =",dut.MUX_used_to_U_type.value)
        
